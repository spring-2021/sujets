# Sujets

Présentation des sujets pour SPRING 2021.


## Théorie des graphes

| Livre | Auteurs | Niveau | ISBN |
| ----- | ------- | ------ | ---- |
| [Introduction to Graph Theory](/Livres/Graphes/Introduction_to_Graph_Theory_by_Richard_J._Trudeau_(z-lib.org).epub) | Richard J. Trudeau | Intro | 9780486678702 |
| [Introductory Graph Theory](/Livres/Graphes/Introductory_Graph_Theory_by_Gary_Chartrand_(z-lib.org).epub) | Gary Chartrand | Intro | 9780486247755 |
| [The Fascinating World of Graph Theory](/Livres/Graphes/The_Fascinating_World_of_Graph_Theory_by_Arthur_Benjamin,_Gary_Chartrand,_Ping_Zhang_(z-lib.org).pdf) | Arthur Benjamin, Gary Chartrand, Ping Zhang | Intermédiaire | 9780691163819 |
| [Pearls in Graph Theory](/Livres/Graphes/Pearls_in_Graph_Theory_A_Comprehensive_Introduction_by_Nora_Hartsfield,_Gerhard_Ringel,_Mathematics_(z-lib.org).epub) | Nora Hartsfield, Gerhard Ringel | Intro plus avancée | 9780486315522 |


## Théorie des nœuds

| Livre | Auteurs | Niveau | ISBN |
| ----- | ------- | ------ | ---- |
| [The Knot Book](/Livres/Nœuds/The_Knot_Book_by_Colin_Adams_(z-lib.org).pdf) | Colin C. Adams | Parfait | 9780821836781 |


## Logique et théorie des ensembles

Principalement de la logique,
mais la théorie des ensembles pourrait faire une discrète apparition.


### Quelques liens

- [Plein de trucs stylés sur Gödel et cie](http://www.godelbook.net/)
- [_World's shortest explanation of Gödel's theorem_](
    https://blog.plover.com/math/Gdl-Smullyan.html)


### Quelques livres


| Livre | Auteurs | Niveau | ISBN |
| ------ | ------ | ------ | ------ |
| [_Mathematical Logic_](/Livres/Logique/Mathematical_Logic_by_Ian_Chiswell,_Wilfrid_Hodges_(z-lib.org).pdf) | Ian Chiswell, Wilfrid Hodges | Logique du premier ordre | 9780198571001 |
| [_A Friendly Introduction to Mathematical Logic_](/Livres/Logique/A_Friendly_Introduction_to_Mathematical_Logic_by_C_2864056__z-lib.org_.pdf) | Christopher C. Leary, Lars Kristiansen | (In)complétude | 9781942341321 |
| [_A Mathematical Introduction to Logic_](/Livres/Logique/A_Mathematical_Introduction_to_Logic__Second_Editi_2082076__z-lib.org_.pdf) | Herbert B. Enderton | (In)complétude et logique du 2e ordre | 9780122384523 |
|  [_Logic and Structure_](/Livres/Logique/Logic_and_Structure_by_Dirk_van_Dalen__2130346__z-lib.org_.pdf) | Dirk van Dalen | Chaud, beaucoup (trop) de détails | 9781447145585 |
| [_Open Logic Project_](https://openlogicproject.org/) | _Open source_ | Large choix, qualité irrégulière | |
| [_A First Course in Logic_](/Livres/Logique/A_First_Course_in_Logic_An_Introduction_to_Model_Theory__Proof_Theory__Computability__and_Complexity_by_Shawn_Hedman__z-lib.org_.pdf) | Shawn Hedman | Théorie des modèles; relativement avancé, mais clair | 9780198529811 |
| [_Propositional and Predicate Calculus_](/Livres/Logique/Propositional_and_Predicate_Calculus__A_Model_of_Argument_by_Derek_Goldrei__z-lib.org_.pdf) | Derek Goldrei | Pas mal; sous-ensemble de Hedman? | 9781852339210 |
| [_Computability and Logic_](/Livres/Logique/Computability_and_Logic_by_George_S._Boolos,_John_P._Burgess,_Richard_C._Jeffrey_(z-lib.org).pdf) | George S. Boolos, John P. Burgess, Richard C. Jeffrey | Axé calculabilité; classique, original | 9780521701464 |


#### [Raymond Smullyan](https://en.wikipedia.org/wiki/Raymond_Smullyan)

Ce dernier a écrit bon nombre de livres de « vulgarisation » des mathématiques,
majoritairement du domaine de la logique et un peu de la théorie des ensembles.
Les trois premiers sont de particulièrement bons candidats pour SPRING.
Je pense que le plus adapté est _Forever Undecided_, mais à voir.

||||
|-|-|-|
| [_Forever Undecided_](/Livres/Logique/Smullyan/Forever_Undecided_A_Puzzle_Guide_to_Godel_by_Raymond_Smullyan_(z-lib.org).pdf) | Gödel | 9780394549439 |
| [_Satan, Cantor, and Infinity_](/Livres/Logique/Smullyan/Satan,_Cantor,_And_Infinity_and_Other_Mind-Boggling_Puzzles_by_Raymond_M._Smullyan_(z-lib.org).djvu) | Infini | 9780307819826 |
| [_The Gödelian Puzzle Book_](/Livres/Logique/Smullyan/The_Gödelian_Puzzle_Book_Puzzles,_Paradoxes_and_Proofs_by_Raymond_M._Smullyan_(z-lib.org).pdf) | Gödel (+infini?) | 9780486497051 |
| [_Gödels Incompleteness Theorems_](/Livres/Logique/Smullyan/Gödels_Incompleteness_Theorems_by_Raymond_M._Smullyan_(z-lib.org).pdf) | Gödel, formellement; trop difficile, mais super intéressant | 9781423735199 |
| [_Diagonalization and Self-Reference_](/Livres/Logique/Smullyan/Diagonalization_and_Self-Reference_by_Raymond_M._S_911653_(z-lib.org).pdf) | (Trop) difficile, mais super intéressant | 9780198534501 |
